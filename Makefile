TARGET = pvrtexture.elf
OBJS = main.o romdisk.o

all: rm-elf $(TARGET)

include $(KOS_BASE)/Makefile.rules

clean:
	-rm -f $(TARGET) $(OBJS) romdisk.* *.bin *.BIN

rm-elf:
	-rm -f $(TARGET)

$(TARGET): $(OBJS)
	kos-cc -o $(TARGET) $(OBJS) -lkmg -lkosutils
	sh-elf-objcopy -R .stack -O binary  $@ $(basename $@).bin

romdisk.img:
	$(KOS_GENROMFS) -f romdisk.img -d romdisk

romdisk.o: romdisk.img
	$(KOS_BASE)/utils/bin2o/bin2o romdisk.img romdisk romdisk.o

.PHONY:scramble
scramble: $(TARGET)
	scramble $(basename $(TARGET)).bin 1ST_READ.BIN
    
run: $(TARGET)
	$(KOS_LOADER) $(TARGET) -n

dist:
	rm -f $(OBJS)
	$(KOS_STRIP) $(TARGET)
