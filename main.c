
/*
 * File: main.c
 * Project: kos_pvr_texture_load
 * File Created: Wednesday, 23rd January 2019 8:07:09 pm
 * Author: Hayden Kowalchuk (hayden@hkowsoftware.com)
 * -----
 * Copyright (c) 2019 Hayden Kowalchuk
 */

#include <stdio.h>
#include <stdlib.h>

#include <dc/pvr.h>
#include <dc/fmath.h>

static pvr_poly_hdr_t hdr;
static pvr_ptr_t txr;

extern uint8 romdisk[];
KOS_INIT_ROMDISK(romdisk);

static pvr_ptr_t load_pvr(char const* filename, uint32* w, uint32* h, uint32* txrFormat) {
	pvr_ptr_t rv;
    
    #define PVR_HDR_SIZE 0x20
    FILE *tex = NULL;
    unsigned char *texBuf;
    unsigned int texSize;

    tex = fopen(filename, "rb");

    if(tex == NULL) {
        printf("Failed to load image file: %s\n", filename);
		return NULL;
	}

    fseek(tex, 0, SEEK_END);
    texSize = ftell(tex);

    texBuf = (unsigned char*)malloc(texSize);
    fseek(tex, 0, SEEK_SET);
    fread(texBuf, 1, texSize, tex);
    fclose(tex);

    int texW = texBuf[PVR_HDR_SIZE - 4] | texBuf[PVR_HDR_SIZE - 3] << 8;
    int texH = texBuf[PVR_HDR_SIZE - 2] | texBuf[PVR_HDR_SIZE - 1] << 8;
    int texFormat, texColor;
    int Bpp=2; // in Bytes

    switch((unsigned int)texBuf[PVR_HDR_SIZE - 8]) {
        case 0x00:
            texColor = PVR_TXRFMT_ARGB1555;
            Bpp = 2;
            break; //(bilevel translucent alpha 0,255)

        case 0x01:
            texColor = PVR_TXRFMT_RGB565;
            Bpp = 2;
            break; //(non translucent RGB565 )

        case 0x02:
            texColor = PVR_TXRFMT_ARGB4444;
            Bpp = 2;
            break; //(translucent alpha 0-255)

        case 0x03:
            texColor = PVR_TXRFMT_YUV422;
            Bpp = 1;
            break; //(non translucent UYVY )

        case 0x04:
            texColor = PVR_TXRFMT_BUMP;
            Bpp = 2;
            break; //(special bump-mapping format)

        case 0x05:
            texColor = PVR_TXRFMT_PAL4BPP;
            Bpp = 1;
            break; //(4-bit palleted texture)

        case 0x06:
            texColor = PVR_TXRFMT_PAL8BPP;
            Bpp = 1;
            break; //(8-bit palleted texture)

        default:
			texColor = PVR_TXRFMT_RGB565;
            Bpp = 2;
            break;
    }
            
    switch((unsigned int)texBuf[PVR_HDR_SIZE - 7]) {
        case 0x01:
            texFormat = PVR_TXRFMT_TWIDDLED;
            break;//SQUARE TWIDDLED

        case 0x03:
            texFormat = PVR_TXRFMT_VQ_ENABLE;
            break;//VQ TWIDDLED

        case 0x09:
            texFormat = PVR_TXRFMT_NONTWIDDLED;
            break;//RECTANGLE

        case 0x0B:
            texFormat = PVR_TXRFMT_STRIDE | PVR_TXRFMT_NONTWIDDLED;
            break;//RECTANGULAR STRIDE

        case 0x0D:
            texFormat = PVR_TXRFMT_TWIDDLED;
            break;//RECTANGULAR TWIDDLED

        case 0x10:
            texFormat = PVR_TXRFMT_VQ_ENABLE | PVR_TXRFMT_NONTWIDDLED;
            break;//SMALL VQ

        default:
            texFormat = PVR_TXRFMT_NONE;
            break;
    }
    
	if(!(rv = pvr_mem_malloc(texW*texH*2))) {
		printf("Couldn't allocate memory for texture!\n");
		free(texBuf);
		return NULL;
	}
    pvr_txr_load(texBuf+PVR_HDR_SIZE, rv, texW*texH*2);
    
	free(texBuf);

	*w = texW;
	*h = texH;
    *txrFormat = texFormat | texColor;

	return rv;
}

static int init() {
	pvr_poly_cxt_t cxt;
	uint32 w, h, imgFormat;

	if(!(txr = load_pvr("/rd/disc.pvr", &w, &h, &imgFormat)))
		return 1;

	pvr_poly_cxt_txr(&cxt, PVR_LIST_OP_POLY, imgFormat, w, h, txr, PVR_FILTER_NONE);
	pvr_poly_compile(&hdr, &cxt);

	return 0;
}

static float max(float a, float b) {
	return a > b ? a : b;
}

static void draw() {
	float const sw = 640, sh = 480;
	float const w = 512, h = 512;
	float const scale = .5f * max(w/sw, h/sh);
	float const x1 = .5f*sw - scale*w, x2 = .5f*sw + scale*w, y1 = .5f*sh - scale*h, y2 = .5f*sh + scale*h;

    pvr_vertex_t vert = {
		.argb = PVR_PACK_COLOR(1.0f, 1.0f, 1.0f, 1.0f),
		.oargb = 0,
		.flags = PVR_CMD_VERTEX,
		.z = 1
	};

	pvr_wait_ready();
	pvr_scene_begin();

	pvr_list_begin(PVR_LIST_OP_POLY);
	pvr_prim(&hdr, sizeof(hdr));

    vert.x = x1;
    vert.y = y1;
    vert.u = 0.0;
    vert.v = 0.0;
    pvr_prim(&vert, sizeof(vert));

    vert.x = x2;
    vert.y = y1;
    vert.u = 1.0;
    vert.v = 0.0;
    pvr_prim(&vert, sizeof(vert));

    vert.x = x1;
    vert.y = y2;
    vert.u = 0.0;
    vert.v = 1.0;
    pvr_prim(&vert, sizeof(vert));

    vert.flags = PVR_CMD_VERTEX_EOL;
    vert.x = x2;
    vert.y = y2;
    vert.u = 1.0;
    vert.v = 1.0;
    pvr_prim(&vert, sizeof(vert));

	pvr_list_finish();

	pvr_scene_finish();
}

int main(int argc, char *argv[]) {
	pvr_init_defaults();

	if(init()) {
		puts("Init error.");
		return 1;
	}

	for(;;) {
		draw();
	}

	pvr_mem_free(txr);

	return 0;
}
